package chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class Threadrecibir implements Runnable{

	private Chat chat;
	private Socket conexion;
	private String persona;
	
	public Threadrecibir(Socket conexion, Chat chat , String persona){
		this.chat=chat;
		this.conexion=conexion;
		this.persona=persona;
	}
	
	public void run() {
		BufferedReader canalEntradaDelServidor = null;
		try {
			canalEntradaDelServidor = new BufferedReader(
					new InputStreamReader(
							conexion.getInputStream()
					)
			);
		} catch (IOException e) { //abortar si hay problemas
			System.err.println("I/O problem: error al conectar");
			System.exit(1);
		}
		while(true){
			try {
				chat.texto.append(persona+canalEntradaDelServidor.readLine()+"\n");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
