package chat;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class Threadescritura implements Runnable{

	private final Chat chat;
	private String buffer;
	private PrintWriter canalSalidaAlServidor = null;
	private String persona;


	
	public Threadescritura(Socket conexion, final Chat chat , String persona){
		this.chat=chat;
		buffer=null;
		this.persona=persona;
		chat.textoentrada.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                 buffer=event.getActionCommand();
                 enviardatos(buffer);
                 chat.textoentrada.setText(""); //borra el texto del enterfield
            } //Fin metodo actionPerformed
        });
		try {
			canalSalidaAlServidor = new PrintWriter(
					conexion.getOutputStream(),
					true
			);

		} catch (IOException e) { //abortar si hay problemas
			System.err.println("I/O problem: error");
			System.exit(1);
		}
	}

	public void enviardatos(String datos){
		canalSalidaAlServidor.println(datos);
		chat.texto.append(persona+datos+"\n");
	}

	@Override
	public void run() {
		
	}

}
