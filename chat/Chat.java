package chat;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class Chat extends JFrame{

	
	private static final long serialVersionUID = 1L;
	private JFrame frame;
	private Container panel;
	JTextArea texto;
	JTextField textoentrada;
	static ServerSocket serverSocket = null;
	static Socket socket = null;
	static String SERVER_ADDRESS;
	static int SERVER_PORT;
	
	static boolean opcion=false;
	static int opcion2;
	static String ip;
	
	public Chat() {
		frame = new JFrame("Chat");
		panel = frame.getContentPane();
		texto = new JTextArea();
		texto.setLineWrap(true);
		texto.setEditable(false);
		textoentrada = new JTextField();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				
		JMenu menuArchivo = new JMenu("Archivo");
	    JMenuItem salir = new JMenuItem("Salir");
	    JMenuItem servidor = new JMenuItem("Conectar como servidor");
	    JMenuItem cliente = new JMenuItem("Conectar como cliente");
	    menuArchivo.add(servidor);
	    menuArchivo.add(cliente);
	    menuArchivo.add(salir); //Agrega el submenu Salir al menu menuArchivo  
	    JMenuBar barra = new JMenuBar(); //Crea la barra de menus
	    setJMenuBar(barra);
	    barra.add(menuArchivo);
	    
	    salir.addActionListener(new ActionListener() { //clase interna anonima
            public void actionPerformed(ActionEvent e) {
                System.exit(0); //Sale de la aplicacion
            }
	    });
	    
	    servidor.addActionListener(new ActionListener(){
	    	public void actionPerformed(ActionEvent e){
	    		opcion2=2;
	    		opcion=true;

	    	}
	    });
	    
	    cliente.addActionListener(new ActionListener(){
	    	public void actionPerformed(ActionEvent e){
	    		opcion2=1;
	    		ip=textoentrada.getText();
	    		opcion=true;
	    	}
	    });
	    
	    panel.add(barra, BorderLayout.NORTH);
	    panel.add(new JScrollPane(texto), BorderLayout.CENTER);
		panel.add(textoentrada, BorderLayout.SOUTH);
		frame.setSize(400, 400);
		frame.setVisible(true);
	}
	
	public void añadir(String datos){
		texto.append(datos+"%n");
	}

	public static void main(String[] args){
		Chat chat=new Chat();
		while(!opcion){
			Thread.yield();
		}
		if(opcion2==1){
			SERVER_ADDRESS = ip;
			SERVER_PORT = 31000;
			boolean exito;
			exito = conectarServidor(10); //10 intentos
			if(!exito){
				System.err.println("Don't know about host:"
						+ SERVER_ADDRESS);
				System.exit(1); //abortar si hay problemas
			}
		}
		else{
			serverSocket = creaListenSocket(31000);
			socket = creaClientSocket(serverSocket);
		}
		Threadescritura escritura = new Threadescritura(socket, chat,"TU>>");
		Threadrecibir recibir = new Threadrecibir(socket, chat,"Otro>>");
		Thread proceso = new Thread(recibir);
		Thread proceso2 = new Thread(escritura);
		proceso.start();
		proceso2.start();
		try {
			proceso.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//Crea un socket de servidor
	//Aborta programa si no lo logra
	private static 
	        ServerSocket creaListenSocket(int serverSockNum){
		ServerSocket server = null;

		try{
    		server = new ServerSocket(serverSockNum);
  		} catch (IOException e) {
   			System.err.println("Problems in port: " + 
			                         serverSockNum);
   			System.exit(-1);
   		}
   		return server;
  	}

  	//Establece conexi√≥n con server y devuelve socket
  	//Aborta programa si no lo logra
  	private static
	        Socket creaClientSocket(ServerSocket server){
  		Socket res = null;

  		try {
			res = server.accept();
		} catch (IOException e) {
			System.err.println("Accept failed.");
			System.exit(1);
		}
		return res;
  	}
  	
  	static private boolean conectarServidor(int maxIntentos){
		//hasta maxIntentos intentos de conexi√≥n, para
		//darle tiempo al servidor a arrancar
		boolean exito = false; //¬øhay servidor?
		int van = 0;

		while((van<maxIntentos) && !exito){
			try {
				socket = new Socket(
					SERVER_ADDRESS, SERVER_PORT);
				exito = true;
			} catch (Exception e) {
				van++;
				System.err.println("Failures:" + van);
				try { //esperar 1 seg
    				Thread.sleep(1000);
				} catch (InterruptedException e2) {
    				e2.printStackTrace();
				}
			}
		}
		return exito;
	}
}
